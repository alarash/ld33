﻿using UnityEngine;
using System.Collections;

public class PlaySoundDelay : MonoBehaviour {

	public float delay = 0;

	// Use this for initialization
	void Start () {
		Invoke ("PlaySound", delay);
	}

	void PlaySound () {
		GetComponent<AudioSource> ().Play ();
	}
}
