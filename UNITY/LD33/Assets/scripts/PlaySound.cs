﻿using UnityEngine;
using System.Collections;

public class PlaySound : MonoBehaviour {

	AudioSource audioSource;

	// Use this for initialization
	void Start () {
		audioSource = GetComponent<AudioSource> ();
	}
	
	public void Play(AudioClip clip){
		audioSource.Stop ();
		audioSource.clip = clip;
		
		audioSource.Play ();
	}
}
