﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//struct TransformAudioClip {
//	public Transform transform;
//	public AudioClip clip;
//
//	public TransformAudioClip(Transform _t, AudioClip _c){
//		transform = _t;
//		clip = _c;
//	}
//}

public class Shouter : MonoBehaviour {
	public static Shouter Instance;

	AudioSource audioSource;
//	Queue<TransformAudioClip> clipList;
//
	AudioClip lastClip = null;
	float lastTime = 0f;
//	bool isPlaying = false;

	
	void Awake(){
		Instance = this;
	}

	// Use this for initialization
	void Start () {
		audioSource = GetComponent<AudioSource>();
		//clipList = new Queue<TransformAudioClip> ();
	}

	public void Shout(GameObject go, AudioClip clip){
		if(audioSource.isPlaying /*&& clip == audioSource.clip*/) return;

		if (clip == lastClip && Time.time - lastTime < 3f)
			return;

		//audioSource.Stop();
		
		transform.parent = go.transform;
		
		audioSource.clip = clip;
		audioSource.Play();

		lastClip = clip;
		lastTime = Time.time;
	}

//
//	public void Shout(GameObject go, AudioClip clip){
//		//if(audioSource.isPlaying) return;
//		//audioSource.Stop();
//		
//		//transform.parent = go.transform;
//		
//		//audioSource.clip = clip;
//		//audioSource.Play();
//		TransformAudioClip tc = new TransformAudioClip (go.transform, clip);
//		clipList.Enqueue(tc);
//
//		if (!isPlaying) {
//			isPlaying = true;
//			Deque ();
//		}
//	}
//
//	void Deque(){
//		Debug.Log ("Shouter:" + clipList.Count);
//
//		if (clipList.Count == 0) {
//			isPlaying = false;
//			return;
//		}
//
//		TransformAudioClip tc = clipList.Dequeue ();
//
//		if (lastClip != null && lastClip == tc.clip) {
//			Invoke ("Deque", 1f);
//
//		} else {
//
//			transform.parent = tc.transform;
//			audioSource.clip = tc.clip;
//			audioSource.Play ();
//
//			Invoke ("Deque", tc.clip.length);
//		}
//
//	}
}
