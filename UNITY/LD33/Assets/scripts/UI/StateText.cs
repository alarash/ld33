﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StateText : MonoBehaviour {

	Text text;
	public EnemyScript entity;

	// Use this for initialization
	void Start () {
		text = GetComponent<Text>();
		
	}
	
	// Update is called once per frame
	void Update () {
		if(entity.FSM == null) return;
		text.text = entity.FSM.CurrentState.ToString();
	}
}
