﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CinematicText : MonoBehaviour {
	public Text text;
	public Text buttonText;

	public string[] textlines;
	public string buttonLabel = "...";
	public string levelName = "play";

	int textIndex = 0;

	void Start () {
		text.text = textlines[0];


	}

	public void Next(){
		textIndex++;
		if(textIndex == textlines.Length){
			Application.LoadLevel(levelName);
		}
		else {
			string textline = textlines[textIndex];
			textline = textline.Replace("|", "\n");
			text.text = textline;

			if(textIndex == textlines.Length - 1)
				buttonText.text = buttonLabel;
		}
	}
}
