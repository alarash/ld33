using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HealthSlider : MonoBehaviour {
	Slider slider;
	public Entity entity;
	
	// Use this for initialization
	void Start () {
		slider = GetComponent<Slider>();
		slider.maxValue = entity.maxHealth;
	}
	
	// Update is called once per frame
	void Update () {
		slider.value = entity.health;
	}
}
