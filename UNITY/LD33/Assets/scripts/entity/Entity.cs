﻿using UnityEngine;
using System.Collections;

public class Entity : MonoBehaviour {

	Rigidbody rigidBody;
	public Animator animator;

	void Awake(){
		maxEnergy = energy;
		maxHealth = health;
	}

	void Start () {
		rigidBody = GetComponent<Rigidbody>();
		animator.SetFloat("MoveY", -1f);
	}

	void FixedUpdate(){
		UpdateMove ();
		attackTimer -= Time.deltaTime;
	}

	#region Energy
	public float energy = 100f;
	[HideInInspector] public float maxEnergy;

	public float moveEnergy = 1f;
	public float idleEnergy = 1f;
	public float attackEnergy = 5f;
	
	public void HurtEnergy(float amount){
		energy -= amount;
		if (energy <= 0f) {
			energy = 0f;
			//Energy 0 event
		}
	}
	
	public void HealEnergy(float amount){
		energy += amount;
		if (energy > maxEnergy) {
			energy = maxEnergy;
		}
	}
	#endregion

	#region Health
	public float health = 100f;
	[HideInInspector] public float maxHealth;

	public void HurtHealth(float amount){
		health -= amount;

		if (health > 0)
			animator.SetTrigger ("Hurt");
		else {
			animator.SetTrigger ("Die");
			animator.SetBool("Dead", true);
		}

		if (health <= 0f) {
			health = 0f;
			//Energy 0 event


		}
	}
	
	public void HealHealth(float amount){
		health += amount;
		if (health > maxHealth) {
			health = maxHealth;
		}
	}

	#endregion

	#region Move
	Vector3 velocity;
	public float speed = 1f;
	public float maxSpeed = 10f;
	public void SetVelocity(Vector3 _velocity){
		velocity = _velocity * speed;
	}

	public bool CanMove(){
		return energy > 0f;//moveEnergy;
	}
	
	public void Move(float x, float z){
		if(!CanMove()) return;
		SetVelocity(new Vector3(x, 0, z));
		HurtEnergy (moveEnergy * Time.deltaTime);

		float moveX = Mathf.Sign(x);
		if(Mathf.Abs(x) < 0.3) moveX = 0;

		float moveY = Mathf.Sign(z);
		if(Mathf.Abs(z) < 0.3) moveY = 0;

		animator.SetFloat("MoveX", moveX);
		animator.SetFloat("MoveY", moveY);
		animator.SetBool("Walk", true);
	}
	
	public void MoveTo(Vector3 pos){
		Vector3 diff = (pos - transform.position).normalized;
		
		Move(diff.x, diff.z);
	}
	
	public void MoveTo(float x, float z){
		MoveTo(new Vector3(x, 0, z));
	}

	public void UpdateMove(){
		rigidBody.AddForce(velocity, ForceMode.Force);
		
		if (rigidBody.velocity.magnitude > maxSpeed){
			rigidBody.velocity = rigidBody.velocity.normalized * maxSpeed;
		}
		
		velocity = Vector3.zero;
	}
	#endregion

	#region Idle
	public void Idle(){
		HealEnergy (idleEnergy * Time.deltaTime);
		animator.SetBool("Walk", false);
	}
	#endregion

	#region Attack
	float attackTimer = 0f;
	public bool CanAttack(){
		return energy >= attackEnergy && attackTimer <= 0f;
	}

	public void Attack(Entity other, float damage){
		if(!CanAttack()) return;

		attackTimer = 1f;
		animator.SetTrigger("Attack");

		other.HurtHealth(damage);
		HurtEnergy (attackEnergy);

	}
	#endregion
	

}
