using UnityEngine;
using System.Collections;

public sealed class SearchState: FSMState<EnemyScript> {
	static readonly SearchState instance = new SearchState();
	public static SearchState Instance {get {return instance;}}
	
	private SearchState(){}
	
	public override void Enter (EnemyScript enemy)
	{
		//Debug.Log("Start Searching");
		enemy.timer = 0f;
		
		PlayerScript.instance.CheckHunted();
		if(!PlayerScript.instance.isHunted && Game.Instance.gameTime > 5f)
			enemy.PlayRandomSound(enemy.searchClips);
	}
	
	public override void Execute (EnemyScript enemy)
	{
		if(enemy.CanSeePlayer()){
			//Debug.Log(entity.ToString() + ": I Can see Player !!!");
			
			enemy.targetPosition = enemy.playerTransform.position;
			enemy.hasTarget = true;

			//if(!PlayerScript.instance.isHunted)
				enemy.PlayRandomSound(enemy.foundClips);
			
			//entity.FSM.ChangeState(ChaseState.Instance);
			enemy.FSM.ChangeState(CallForHelpState.Instance);
		}
		else {
			//WANDER
			enemy.timer += Time.deltaTime;
			if(enemy.timer > 2f){
				enemy.timer = 0;
				
				Vector3 randPos = new Vector3(
					enemy.transform.position.x + (Random.value * 4f) - 2f,
					0f,
					enemy.transform.position.z + (Random.value * 4f) - 2f
					);
				
				enemy.targetPosition = randPos;
				enemy.hasTarget = true;
			}
			
			if(enemy.hasTarget)
				enemy.MoveTo(enemy.targetPosition);
			
		}
	}
	
	public override void Exit (EnemyScript enemy)
	{
		//Debug.Log("Stop Searching");
	}
}