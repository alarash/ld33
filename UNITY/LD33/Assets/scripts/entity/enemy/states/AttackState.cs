using UnityEngine;
using System.Collections;

public sealed class AttackState: FSMState<EnemyScript> {
	static readonly AttackState instance = new AttackState();
	public static AttackState Instance {get {return instance;}}
	
	private AttackState(){}
	
	public override void Enter (EnemyScript enemy)
	{
		PlayerScript.instance.CheckHunted();
	}
	
	public override void Execute (EnemyScript enemy)
	{

		if(!enemy.IsNearPlayer()){
			enemy.FSM.ChangeState(ChaseState.Instance);
			return;
		}

		//if (enemy.entity.CanAttack ())
			enemy.AttackPlayer ();
		//else {
		//	enemy.FSM.ChangeState(IdleState.Instance);
		//}
		if (enemy.entity.energy < enemy.entity.attackEnergy)
			enemy.FSM.ChangeState (IdleState.Instance);
		

		
	}
	
	public override void Exit (EnemyScript enemy)
	{
		
	}
}