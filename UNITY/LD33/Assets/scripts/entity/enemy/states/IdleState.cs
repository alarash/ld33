using UnityEngine;
using System.Collections;

public sealed class IdleState: FSMState<EnemyScript> {
	static readonly IdleState instance = new IdleState();
	public static IdleState Instance {get {return instance;}}
	
	private IdleState(){}
	
	public override void Enter (EnemyScript enemy)
	{
		enemy.hasTarget = false;

		enemy.GetComponent<Rigidbody> ().isKinematic = true;
	}
	
	public override void Execute (EnemyScript enemy)
	{

		enemy.entity.Idle();
		if(enemy.FSM == null) return;
		if (enemy.entity.energy > enemy.entity.maxEnergy * 0.5f) {
			enemy.FSM.ChangeState(enemy.FSM.PreviousState);
		}
		
	}
	
	public override void Exit (EnemyScript enemy)
	{
		enemy.GetComponent<Rigidbody> ().isKinematic = false;
	}
}