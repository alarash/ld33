using UnityEngine;
using System.Collections;

public sealed class EnemyGlobalState: FSMState<EnemyScript> {
	static readonly EnemyGlobalState instance = new EnemyGlobalState();
	public static EnemyGlobalState Instance {get {return instance;}}
	
	private EnemyGlobalState(){}
	
	public override void Enter (EnemyScript enemy)
	{
		
	}
	
	public override void Execute (EnemyScript enemy)
	{
		//Debug.Log("Enemy Global State...");
		if (enemy.entity.energy == 0f)
			enemy.FSM.ChangeState (IdleState.Instance);

		if (!PlayerScript.instance.isMonster || PlayerScript.instance.isDead) {
			enemy.FSM.ChangeState (IdleState.Instance);
			enemy.FSM = null;
		}
		
	}
	
	public override void Exit (EnemyScript enemy)
	{
		
	}
}