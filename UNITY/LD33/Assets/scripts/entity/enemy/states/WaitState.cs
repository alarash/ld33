using UnityEngine;
using System.Collections;

public sealed class WaitState: FSMState<EnemyScript> {
	static readonly WaitState instance = new WaitState();
	public static WaitState Instance {get {return instance;}}
	
	private WaitState(){}
	
	public override void Enter (EnemyScript enemy)
	{
		enemy.timer = 0;
	}
	
	public override void Execute (EnemyScript enemy)
	{
		enemy.timer += Time.deltaTime;
		if (enemy.timer > 4f) {
			enemy.FSM.ChangeState(enemy.FSM.NextState);
		}
	}
	
	public override void Exit (EnemyScript enemy)
	{
		
	}
}