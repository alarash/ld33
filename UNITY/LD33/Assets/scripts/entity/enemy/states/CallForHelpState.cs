using UnityEngine;
using System.Collections;


public sealed class CallForHelpState: FSMState<EnemyScript> {
	static readonly CallForHelpState instance = new CallForHelpState();
	public static CallForHelpState Instance {get {return instance;}}
	
	private CallForHelpState(){}
	
	public override void Enter (EnemyScript enemy)
	{
		enemy.timer = 0f;
		enemy.CallForHelp();
	}
	
	public override void Execute (EnemyScript enemy)
	{
		enemy.timer += Time.deltaTime;
		if (enemy.timer > 1f) {
			enemy.FSM.ChangeState(ChaseState.Instance);
		}
		
		
	}
	
	public override void Exit (EnemyScript enemy)
	{

	}
}