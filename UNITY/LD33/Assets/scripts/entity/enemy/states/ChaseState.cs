using UnityEngine;
using System.Collections;


public sealed class ChaseState: FSMState<EnemyScript> {
	static readonly ChaseState instance = new ChaseState();
	public static ChaseState Instance {get {return instance;}}
	
	private ChaseState(){}
	
	public override void Enter (EnemyScript enemy)
	{
		//Debug.Log("Start Chase");
		enemy.targetPosition = enemy.playerTransform.position;
		enemy.timer = 0f;
		
		PlayerScript.instance.CheckHunted();
	}
	
	public override void Execute (EnemyScript enemy)
	{
		if(enemy.hasTarget){
			enemy.MoveTo(enemy.targetPosition);
			if(enemy.IsNear(enemy.targetPosition))
				enemy.hasTarget = false;
		}
		
		if(enemy.CanSeePlayer()){
			enemy.targetPosition = enemy.playerTransform.position;
			enemy.hasTarget = true;
			
			if(enemy.IsNear(enemy.playerTransform.position))
				enemy.FSM.ChangeState(AttackState.Instance);
		}
		else {
			enemy.timer += Time.deltaTime;
			if(!enemy.hasTarget || enemy.timer > 6f){
				if(!PlayerScript.instance.isHunted)
					enemy.PlayRandomSound(enemy.lostClips);
				enemy.FSM.ChangeState(SearchState.Instance);
			}
		}

		
	}
	
	public override void Exit (EnemyScript enemy)
	{
		//Debug.Log("Stop Chase");
	}
}