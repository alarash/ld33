﻿using UnityEngine;
using System.Collections;

public class EnemyScript : MonoBehaviour {
	public FiniteStateMachine<EnemyScript> FSM;
	public Entity entity;
	
	public Transform playerTransform;
	
	public bool hasTarget = false;
	public Vector3 targetPosition;
	
	public float timer = 0f;

	AudioSource audio;

	public AudioClip[] searchClips;
	public AudioClip[] chaseClips;
	public AudioClip[] lostClips;
	public AudioClip[] foundClips;
	public AudioClip[] attackClips;


	// Use this for initialization
	void Start () {
		entity = GetComponent<Entity>();
		audio = GetComponent<AudioSource> ();
		
		playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
		
		FSM = new FiniteStateMachine<EnemyScript>(this, WaitState.Instance/*SearchState.Instance*/);
		FSM.NextState = SearchState.Instance;
		FSM.GlobalState = EnemyGlobalState.Instance;

		entity.energy = Random.Range (70f, 130f);

	}

	public void Update(){
		if(FSM != null)
			FSM.Update();
	}
	
	#region Move
	public void MoveTo(Vector3 pos){
		entity.MoveTo(pos);
	}
	
	public bool IsNear(Vector3 pos){
		return (transform.position - pos).magnitude < 2f;
	}

	public bool IsNearPlayer(){
		return IsNear (playerTransform.position);
	}
	#endregion
	
	#region View
	public float baseViewDistance = 10f;
	public LayerMask obstacleMask;
	public float ViewDistance(){
		return baseViewDistance;
	}
	
	public float DistanceToPlayer(){
		return (transform.position - playerTransform.position).magnitude;
	}
	
	public bool CanSeePlayer(){
		if(DistanceToPlayer() < ViewDistance()){
			RaycastHit hit;
			if(!Physics.Linecast(transform.position, playerTransform.position, out hit, obstacleMask)){
				return true;
			}
		}
		
		return false;
	}
	#endregion

	#region Attack
	//public float attackTimer = 0f;

	public void AttackPlayer(){
		//attackTimer = 3f;
		//PlayerScript.instance.entity.HurtHealth(10f);

		PlayRandomSound (attackClips);

		entity.Attack (PlayerScript.instance.entity, 10f);
	}
	#endregion

	public void CallForHelp ()
	{
		Debug.Log ("Help !!! I Found the monster !!!");
	}

	public void PlayRandomSound(AudioClip[] clips){
		audio.Stop();
		if(clips.Length == 0) return;
		AudioClip clip = clips[Random.Range(0, clips.Length)];
		
		Shouter.Instance.Shout(gameObject, clip);

		//audio.clip = clip;
		//audio.Play();
	}
}

