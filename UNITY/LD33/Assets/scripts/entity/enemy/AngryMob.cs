﻿using UnityEngine;
using System.Collections;

public class AngryMob : MonoBehaviour {
	public GameObject prefab;
	public int number;
	
	void Start () {
		for(int i=0; i < number; i++){
			Vector3 pos = new Vector3(
				Random.value * 10f - 5f,
				0.5f,
				Random.value * 10f - 5f
			);

			//GameObject go = Instantiate(prefab, transform.position + pos, Quaternion.identity) as GameObject;
			Instantiate(prefab, transform.position + pos, Quaternion.identity);
		}
	}
	

}
