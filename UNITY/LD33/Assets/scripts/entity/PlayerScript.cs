﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections;

public class PlayerScript : MonoBehaviour {
	public static PlayerScript instance;

	public Entity entity;

	public float transformationTime = 6f;
	public ParticleSystem transformPS;
	public bool isMonster = true;

	public bool isHunted = false;

	public bool isDead = false;

	public AudioMixerSnapshot noMusic;

	
	void Awake(){
		instance = this;
	}
	
	void Start () {
		entity = GetComponent<Entity>();

	}
	
	void Update () {
		if (!isMonster || isDead)
			return;

		if (entity.health == 0) {
			StartCoroutine(Die());
			return;
		}
			

		if(!isDead) UpdateMove();

		if (Game.Instance.dayTime() < 12f && Game.Instance.dayTime() > transformationTime) {
			StartCoroutine(TransformToHuman());
		}
		
	}
	
	void UpdateMove(){
		float horizontal = Input.GetAxisRaw("Horizontal");
		float vertical = Input.GetAxisRaw("Vertical");
		
		if(horizontal != 0 || vertical != 0){	
			entity.Move(horizontal, vertical);
		}
		else {
			entity.Idle();
		}
	}
	
	public void CheckHunted(){
		Debug.Log("CheckHunted");
		
		isHunted = false;
		foreach(EnemyScript es in GameObject.FindObjectsOfType<EnemyScript>()){
			if(es.FSM == null) continue;
			
			if(
				es.FSM.CurrentState == ChaseState.Instance || 
				es.FSM.CurrentState == AttackState.Instance){
				isHunted = true;
				break;	
			}	
		}
		
		Debug.Log(isHunted);
		
		if(isHunted){
			MusicScript.Instance.ToHunted();
		}
		else {
			MusicScript.Instance.ToCalm();
		}
	}

	IEnumerator TransformToHuman ()
	{
		noMusic.TransitionTo (0.1f);

		isMonster = false;
		transformPS.Play ();
		entity.animator.SetTrigger ("Human");

		yield return new WaitForSeconds (5f);

		if(isHunted)
			Application.LoadLevel ("outro_captured");
		else
			Application.LoadLevel ("outro_win");

	}

	IEnumerator Die(){
		noMusic.TransitionTo (1.0f);

		isDead = true;

		yield return new WaitForSeconds (5f);

		Application.LoadLevel("outro_killed");
	}


}
