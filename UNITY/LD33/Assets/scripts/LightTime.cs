﻿using UnityEngine;
using System.Collections;

public class LightTime : MonoBehaviour {
	Light light;

	public Color dayColor;
	public Color nightColor;

	public float dayShadow;
	public float nightShadow;

	[Range(0f, 1f)]
	public float ratio = 0f;


	// Use this for initialization
	void Start () {
		light = GetComponent<Light>();
	}
	
	// Update is called once per frame
	void Update () {
		ratio = Game.Instance.daylightRatio ();

		//float ratio = 0.5f;
		light.color = Color.Lerp (nightColor, dayColor, ratio); 
		light.shadowStrength = Mathf.Lerp (nightShadow, dayShadow, ratio);
	}
}
