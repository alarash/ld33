﻿using UnityEngine;
using System.Collections;

public class SpriteColorVariation : MonoBehaviour {
	SpriteRenderer spriterenderer;

	public Vector3 min;
	public Vector3 max;

	// Use this for initialization
	void Start () {
		spriterenderer = GetComponent<SpriteRenderer> ();

		Color color = Color.white;
		color.r = Random.Range(min.x, max.x);
		color.g = Random.Range(min.y, max.y);
		color.b = Random.Range(min.x, max.z);

		spriterenderer.color = color;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
