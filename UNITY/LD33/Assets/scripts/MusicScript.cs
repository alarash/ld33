﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class MusicScript : MonoBehaviour {
	public static MusicScript Instance;
	
	public AudioMixerSnapshot calm;
	public AudioMixerSnapshot hunted;
	
	void Awake(){
		Instance = this;
	}
	
	void Start(){
		calm.TransitionTo(0.1f);
	}
	
	public void ToCalm(){
		calm.TransitionTo(2.0f);
	}
	
	public void ToHunted(){
		hunted.TransitionTo(0.5f);
	}
	
}
