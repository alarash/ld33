﻿using UnityEngine;
using System.Collections;

public class Game : MonoBehaviour {
	static public Game Instance;

	public float gameTime = 0f;

	public float time = 0f;
	public float hourLength = 5f;

	void Awake() {
		Instance = this;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		gameTime += Time.deltaTime;
		time += Time.deltaTime / hourLength;
	}

	public float dayTime(){
		return time % 24f;
	}

	public float daylightRatio(){
		//18 --> 22: 1 --> 0
		//2 --> 6: 0 --> 1
		float sunSetStart = 18f;
		float sunSetStop = 22f;

		float sunRiseStart = 4f;
		float sunRiseStop = 8f;

		float t = dayTime();
		if (t > sunRiseStop && t < sunSetStart)
			return 1f;
		else if (t > sunSetStop || t < sunRiseStart)
			return 0f;
		else if (t > sunSetStart) {
			return 1f - ((t - sunSetStart) / (sunSetStop - sunSetStart));	
		}
		else if (t > sunRiseStart) {
			return (t - sunRiseStart) / (sunRiseStop - sunRiseStart);
		}

		return 0.5f;
	}

	public string GetTimeTxt(){
		int hours = (int)dayTime();
		int mins = (int)(60 * (dayTime() - hours));

		return (hours < 10 ? "0":"") + hours.ToString() + ":" + (mins < 10 ? "0":"") + mins.ToString();
	}
}
