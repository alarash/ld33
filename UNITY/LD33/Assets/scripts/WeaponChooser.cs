﻿using UnityEngine;
using System.Collections;

public class WeaponChooser : MonoBehaviour {

	public GameObject[] weapons;
	public Transform anchor;

	// Use this for initialization
	void Start () {
		GameObject weaponPrefab = weapons [Random.Range(0, weapons.Length)];
		if(weaponPrefab == null) return;
		GameObject go = Instantiate (weaponPrefab) as GameObject;

		go.transform.parent = anchor;
		go.transform.localPosition = Vector3.zero;
		go.transform.localRotation = Quaternion.identity;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
