﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class CastShadow : MonoBehaviour {

	SpriteRenderer sr;
	public bool shadowCastingMode = true;
	public bool receiveShadows = true;


	/*void Start(){
		sr = GetComponent<SpriteRenderer>();
		
		sr.shadowCastingMode = shadowCastingMode ? UnityEngine.Rendering.ShadowCastingMode.TwoSided: UnityEngine.Rendering.ShadowCastingMode.Off;
		sr.receiveShadows = receiveShadows;
	}*/


	// Update is called once per frame
	void Update () {
		if(sr == null) sr = GetComponent<SpriteRenderer>();

		sr.shadowCastingMode = shadowCastingMode ? UnityEngine.Rendering.ShadowCastingMode.TwoSided: UnityEngine.Rendering.ShadowCastingMode.Off;
		sr.receiveShadows = receiveShadows;
	}
}
