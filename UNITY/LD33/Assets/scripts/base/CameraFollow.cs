﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	public Transform target;
	public float speed = 1f;
	
	public Vector3 offset;
	
	public float velocityRatio = 0f;
	Rigidbody targetRigidBody;
	
	void Start(){
		targetRigidBody = target.GetComponent<Rigidbody>();
	}


	
	// Update is called once per frame
	void FixedUpdate () {
		//if(target == null) return;
		
		Vector3 pos = Vector3.Lerp(transform.position, target.position + offset, Time.deltaTime * speed);
		if(velocityRatio > 0 && targetRigidBody != null){
			pos += targetRigidBody.velocity * velocityRatio * Time.deltaTime;
		}
		
		transform.position = pos;
	}
}
