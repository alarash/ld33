﻿using UnityEngine;
using System.Collections;

public class AutoDestroy : MonoBehaviour {
	public float time;

	void Start () {
		Destroy (gameObject, time);
	}
}
