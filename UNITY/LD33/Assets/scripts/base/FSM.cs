using UnityEngine;
using System.Collections;

/*
From http://playmedusa.com/blog/a-finite-state-machine-in-c-for-unity3d/
*/

abstract public class FSMState  <T>{
	abstract public void Enter (T entity);
	abstract public void Execute (T entity);
	abstract public void Exit(T entity);
}

public class FiniteStateMachine <T>  {
	private T Owner;
	
	public FSMState<T> GlobalState = null;
	
	public FSMState<T> CurrentState = null;
	public FSMState<T> PreviousState = null;
	public FSMState<T> NextState = null;
	
	public FiniteStateMachine(){}
	public FiniteStateMachine(T owner, FSMState<T> InitialState)
		:this()
	{
		Configure(owner, InitialState);
	}
	public FiniteStateMachine(T owner)
		:this()
	{
		Configure(owner);
	}
	
	public void Awake() {
		CurrentState = null;
		PreviousState = null;
	}
	
	public void Configure(T owner){
		Owner = owner;
	}
	
	public void Configure(T owner, FSMState<T> InitialState) {
		Owner = owner;
		ChangeState(InitialState);
	}
	
	public void  Update() {
		if(GlobalState != null)
			GlobalState.Execute(Owner);
		
		if (CurrentState != null)
			CurrentState.Execute(Owner);
	}
	
	public void  ChangeState(FSMState<T> NewState) {
		PreviousState = CurrentState;
		
		if (CurrentState != null)
			CurrentState.Exit(Owner);
		
		CurrentState = NewState;
		
		if (CurrentState != null)
			CurrentState.Enter(Owner);
	}
	
	public void  RevertToPreviousState() {
		if (PreviousState != null)
			ChangeState(PreviousState);
	}
};